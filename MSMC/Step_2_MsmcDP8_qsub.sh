#! /bin/bash

#$ -cwd
#$ -l highp,h_rt=30:00:00,h_data=2G
#$ -N msmc_Ne_DP8
#$ -pe shared 16
#$ -o /u/flashscratch/flashscratch2/r/rturba/stickleback/scripts/run_logs/msmc_Ne_DP8.out
#$ -e /u/flashscratch/flashscratch2/r/rturba/stickleback/scripts/run_logs/msmc_Ne_DP8.err
#$ -m abe
#$ -M rturba

# run MSMC with default settings (for now)

source /u/local/Modules/default/init/modules.sh
module load python/3.6.1 ## has to be 3.6! otherwise won't work. 

msmc_tools=$HOME/programs/msmc-tools
msmc=$HOME/bin/msmc

wd=$SCRATCH/stickleback/

mkdir -p $wd/msmc_Ne_DP8

indir=$wd/msmcDP8
outdir=$wd/msmc_Ne_DP8

$msmc -t 16 $indir/*_8116_*.txt -o $outdir/8116_msmcDP8.out
$msmc -t 16 $indir/*_8117_*.txt -o $outdir/8117_msmcDP8.out
$msmc -t 16 $indir/*_8218_*.txt -o $outdir/8218_msmcDP8.out
$msmc -t 16 $indir/*_8220_*.txt -o $outdir/8220_msmcDP8.out
$msmc -t 16 $indir/*_CSC0387.1_*.txt -o $outdir/CSC0387.1_msmcDP8.out
$msmc -t 16 $indir/*_CSC0387.2_*.txt -o $outdir/CSC0387.2_msmcDP8.out
$msmc -t 16 $indir/*_CSC0832.1_*.txt -o $outdir/CSC0832.1_msmcDP8.out
$msmc -t 16 $indir/*_CSC0832.2_*.txt -o $outdir/CSC0832.2_msmcDP8.out
$msmc -t 16 $indir/*_DKJ06.2_*.txt -o $outdir/DKJ06.2_msmcDP8.out
$msmc -t 16 $indir/*_DKJ06.3_*.txt -o $outdir/DKJ06.3_msmcDP8.out
$msmc -t 16 $indir/*_RNF2887.1_*.txt -o $outdir/RNF2887.1_msmcDP8.out
$msmc -t 16 $indir/*_RNF2887.2_*.txt -o $outdir/RNF2887.2_msmcDP8.out

