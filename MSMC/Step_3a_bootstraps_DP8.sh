### Want to figure out bootstrapping MSMC
module load python/3.6.8
# this is really fast, can do it in the shell 
# do 50 bootstraps:

wd=/u/scratch/r/rturba/stickleback/msmc
msmc_tools=/u/home/r/rturba/programs/msmc-tools
msmc=/u/home/r/rturba/bin/msmc

# chunk size: default 5000000
# chunks per chrom: how many chunks on a boot strap chrom (20)
# number of chromosomes (30)
# n: num of bootstraps (20)
# seed: initialize random seed  (not sure if need to set this)

#mkdir -p $wd/boot/boot_DP8

# doing 23 chromosomes, (each of 100Mb) so it's 2Gb of sequence, same as the 220 scaffolds
#$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_8116_DP8 $wd/multiHetSep_DP8/*8116*.txt

#$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_8117_DP8 $wd/multiHetSep_DP8/*8117*.txt

#$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_8218_DP8 $wd/multiHetSep_DP8/*8218*.txt

#$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_8220_DP8 $wd/multiHetSep_DP8/*8220*.txt

$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_CCS0387.1_DP8 $wd/multiHetSep_DP8/*CSC0387.1*.txt

$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_CCS0387.2_DP8 $wd/multiHetSep_DP8/*CSC0387.2*.txt

$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_CCS0832.1_DP8 $wd/multiHetSep_DP8/*CSC0832.1*.txt

$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_CCS0832.2_DP8 $wd/multiHetSep_DP8/*CSC0832.2*.txt

#$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_DKJ06.2_DP8 $wd/multiHetSep_DP8/*DKJ06.2*.txt

#$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_DKJ06.3_DP8 $wd/multiHetSep_DP8/*DKJ06.3*.txt

#$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_RNF2867.1_DP8 $wd/multiHetSep_DP8/*RNF2887.1*.txt

#$msmc_tools/multihetsep_bootstrap.py -n 100 --nr_chromosomes 23 $wd/boot/boot_DP8/boot_RNF2867.2_DP8 $wd/multiHetSep_DP8/*RNF2887.2*.txt
