#! /bin/bash

#$ -l highp,h_rt=10:00:00,h_data=10G
#$ -N prepMsmcDP8
#$ -cwd
#$ -m bea
#$ -o /u/flashscratch/flashscratch2/r/rturba/stickleback/scripts/run_logs/prepMsmcDP8.out
#$ -e /u/flashscratch/flashscratch2/r/rturba/stickleback/scripts/run_logs/prepMsmcDP8.err
#$ -M rturba
#$ -t 1-276:1

source /u/local/Modules/default/init/modules.sh

module load python/3.4
module load perl

msmc_tools=$HOME/programs/msmc-tools
msmc=$HOME/bin/msmc

wd=$SCRATCH/stickleback/selVarDP8_out
i=${SGE_TASK_ID}
vcf=$wd/${i}_*.vcf.gz

mkdir -p $SCRATCH/stickleback/msmcDP8
outdir=$SCRATCH/stickleback/msmcDP8


for filename in $vcf ; do
	basename=$( basename ${filename} .vcf.gz)
	$msmc_tools/generate_multihetsep.py $vcf > $outdir/${basename}_MultiHetSep.txt
done

sleep 5m

