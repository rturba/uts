#! /bin/bash

#$ -cwd
#$ -l h_rt=10:00:00,h_data=4G,highp
#$ -N MsmcOnBoot_CCS0832.1DP8
#$ -pe shared 5
#$ -o /u/home/r/rturba/scripts/run_logs/$JOB_NAME_$JOB_ID.out
#$ -e /u/home/r/rturba/scripts/run_logs/$JOB_NAME_$JOB_ID.err
#$ -m abe
#$ -M rturba
#$ -t 1-100:1

source /u/local/Modules/default/init/modules.sh
module load python/3.6.8

wd=/u/scratch/r/rturba/stickleback/msmc/boot
msmc_tools=/u/home/r/rturba/programs/msmc-tools
msmc=/u/home/r/rturba/bin/msmc
i=${SGE_TASK_ID}

# array of the number of bootstraps
$msmc -t 5 $wd/boot_DP8/boot_CCS0832.1_DP8_${SGE_TASK_ID}/*.txt -o $wd/bootOut_DP8/boot_CCS0832.1_DP8_${SGE_TASK_ID}.out